<!DOCTYPE html>
<html>
<head>
    <title>#WebCamp2016</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
    <section class="header">
        <h1>WebCamp 2016</h1>
    </section>
    <section class="about">
        <div>
            <iframe width="100%" height="500" src="https://www.youtube.com/embed/-1Y2WfcCb4M?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <div>
            <p>Join the hippest Web Development conference in the region!</p>

            <p>WebCamp is a two-day seminar and hackathon event that
            focuses on providing technical workshops to various community
            members. It was developed to help boost IT Skills in the region, by
            providing a platform for knowledge sharing, collaboration, and skill
            development.</p>

            <p>This year, we will put more focus on workshops instead of seminars.
            More hands-on experience, blah blah blah</p>
        </div>
    </section>
    <section class="talks">
        <div class="angular">
            <img src="https://camo.githubusercontent.com/6795c053f2fafee4d1c76f3a181876013827dd5e/68747470733a2f2f662e636c6f75642e6769746875622e636f6d2f6173736574732f333437303430322f313230383630372f32376637643134322d323564362d313165332d386330372d6139316532633736396435322e706e67">
            <h2>Angular JS</h2>
            <p>HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop.</p>
            <ul class="speakers">
                <li class="main">
                    <img src="">
                    <h2>John Roca</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="">
                    <h2>Christopher Cinco</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="http://www.gaplabs.ph/upload/staff/staff_54c1df73c6d4093.jpeg">
                    <h2>Neri Celeste</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="">
                    <h2>John Gicain</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
            </ul>
        </div>
        <div class="meteor">
            <img src="https://www.codementor.io/assets/tutorial_icon/meteor.png">
            <h2>Meteor JS</h2>
            <p>Meteor gives you a radically simpler way to build realtime mobile and web apps, entirely in JavaScript from one code base.</p>
            <ul class="speakers">
                <li class="main">
                    <img src="http://www.gaplabs.ph/upload/staff/staff_55fa5ad205249117.jpeg">
                    <h2>Kurt Obispo</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="">
                    <h2>Ferdie Amano</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="">
                    <h2>Raymart Medroso</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <div class="losange">
      <div class="los1">
        <img src="http://farm3.staticflickr.com/2178/3531465579_8bff044e9b_z.jpg?zz=1" alt="" width="255" height="320" />
      </div>
    </div>
                    <img src="http://www.gaplabs.ph/upload/staff/staff_55f76f8a0c43c147.jpeg">
                    <h2>JP Ada</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
            </ul>
        </div>
        <div class="vue">
            <img src="http://vuejs.org/images/logo.png">
            <h2>Vue JS</h2>
            <p>Intuitive, Fast and Composable MVVM for building interactive interfaces.</p>
            <ul class="speakers">
                <li class="main">
                    <img src="">
                    <h2>Clyde Alegro</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="">
                    <h2>Dess Dumali</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="http://www.gaplabs.ph/upload/staff/staff_562059f6c68fd150.jpeg">
                    <h2>Jare Lagarto</h2>
                    <a href="" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="" class="facebook">x</a>
                </li>
                <li>
                    <img src="">
                    <h2>Adam Navarro</h2>
                    <a href="" class="twitter">x</a>
                    <a href="" class="facebook">x</a>
                </li>
            </ul>
        </div>
    </section>
    <section class="schedule">
        <h2>Event Schedule</h2>
        <div>
            <h3>
                <span>Day 1</span>
                February 20, 2016 &middot; Saturday
            </h3>
            <ul>
                <li>
                    <div><span>7:00</span> AM</div>
                    <div>Registration</div>
                </li>
                <li>
                    <div><span>8:00</span> AM</div>
                    <div>Opening</div>
                </li>
                <li>
                    <div><span>12:00</span> PM</div>
                    <div>Lunch Break</div>
                </li>
            </ul>
        </div>
        <div>
            <h3>
                <span>Day 2</span>
                February 21, 2016 &middot; Sunday
            </h3>
            <ul>
                <li>
                    <div><span>7:00</span> AM</div>
                    <div>Registration</div>
                </li>
                <li>
                    <div><span>8:00</span> AM</div>
                    <div>Opening</div>
                </li>
                <li>
                    <div><span>12:00</span> PM</div>
                    <div>Lunch Break</div>
                </li>
            </ul>
        </div>
    </section>
    <section class="faq">
        <h2>FAQ</h2>
        <ul>
            <li class="open">
                <i class="fa fa-caret-down"></i>
                <p class="q">For whom is this conference?</p>
                <p class="a">Web designers, front-end designers & developers, user interface designers, interaction designers, mobile designers, user experience designers, graphic designers, web developers, webmasters, bloggers/marketers/social media managers who do web design, media practitioners, students in IT or visual communication, and other related professions. If you work on web and mobile interfaces, FFC is for you.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">Why should I attend Webcamp 2016?</p>
                <p class="a">You’ll hear from amazing speakers who are the best in the business. Not only are they updated with what’s new in the industry, they’re the trailblazers and innovators themselves–and they’re sharing their wealth of knowledge with you! Every year, we get attendees coming up to us & telling us that they’re on information overload and that they wouldn’t have known of the new technologies available to them if they hadn’t attended the FFC conference. That’s what we want, to nudge our attendees in the right direction so they remain globally competitive in an industry that’s incredibly fast paced.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">Why is the event not free?</p>
                <p class="a">FFC takes months of planning, meetings, and actual expenses. PWDO, as a non-profit organization, doesn’t make a dime out of the ticket costs. Everything goes to the next FFC and other projects that PWDO does for the community.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">What is the Masterclass all about?</p>
                <p class="a">For the first time ever, the Philippine Web Designers Organization is introducing the Masterclass, an exclusive session where participants can immerse themselves in a must-learn topic taught by a speaker at the forefront of the industry. This year’s Masterclass features Responsive Design with Brad Frost and will be held on November 15 at 2nd Floor Function Hall of Hive Hotel, covering the following topics:</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">Can I pay offline?</p>
                <p class="a">Payments for other ticket types will be ignored. Payments made after November 7 will be ignored.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">Do you offer group discounts?</p>
                <p class="a">A group registration ticket that gives you 10 tickets for the price of 9 is available online.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">Do I need to bring my laptop?</p>
                <p class="a">The FFC talks do not require participants to design or code on their laptops, but that is up to you.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">Can I bring a Molotov or start a fire inside the venue?</p>
                <p class="a">No, please don’t. Rather than starting a fire, we encourage you to practice & promote web standards. We appreciate the enthusiasm though.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">Are you a legitimate organization?</p>
                <p class="a">Yes, the Philippine Web Designers Organization, Inc. has been registered with the SEC as a non-profit organization since 2009, with BIR TIN 007-274-276.</p>
            </li>
            <li>
                <i class="fa fa-caret-right"></i>
                <p class="q">How can I help promote FFC?</p>
                <p class="a">You’re awesome! We’ll share promotional images soon.</p>
            </li>
        </ul>
    </section>
    <section class="location">
        <h2>Location</h2>
        <p>Where in the world are we?</p>

        <div>
            <ul>
                <li><h3>How to get there</h3></li>
                <li>Ride a jeepney</li>
            </ul>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15654.87709788583!2d124.99745795112042!3d11.208393991990143!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3308770379c8731b%3A0xc9ccaff436f9f985!2sGAPLabs!5e0!3m2!1sen!2sph!4v1450753299264" width="100%" height="600" frameborder="0" style="border:0;pointer-events:none;" allowfullscreen></iframe>
        </div>

        
    </section>
    <section class="register">
        <h2>Register Now</h2>
        <form>
            <label>Full Name</label>
            <input type="text" />
            <label>Email Address</label>
            <input type="email" />
            <label>Select Track</label>
            <div class="tracks">
                <label><input type="radio" name="optradio"> <span>Angular JS</span></label>
                <label><input type="radio" name="optradio"> <span>Meteor JS</span></label>
                <label><input type="radio" name="optradio"> <span>Vue JS</span></label>
            </div>

            <input type="submit" />
        </form>
    </section>
</body>
</html>